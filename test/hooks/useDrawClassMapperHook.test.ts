import { describe, expect, it } from "vitest";
import useDrawClassMapperHook from "../../src/hooks/useDrawClassMapperHook";

describe("useDrawClassMapperHook", () => {
  const dic: { [key: number]: string } = {
    1: "5 correct + 2 euro numbers",
    2: "5 correct + 1 euro number",
    3: "5 correct",
    4: "4 correct + 2 euro numbers",
    5: "4 correct + 1 euro number",
    6: "4 correct",
    7: "3 correct + 2 euro numbers",
    8: "2 correct + 2 euro numbers",
    9: "3 correct + 1 euro number",
    10: "3 correct",
    11: "1 correct + 2 euro numbers",
    12: "2 correct + 1 euro number",
  };

  it("should return the correct string for each class", () => {
    for (const [key, value] of Object.entries(dic)) {
      expect(useDrawClassMapperHook(parseInt(key))).toEqual(value);
    }
  });

  it("should throw an error if the given class is invalid", () => {
    expect(() => useDrawClassMapperHook(99)).toThrow();
  });
});
