import { describe, expect, it } from "vitest";
import useParseStringDateHook from "../../src/hooks/useParseStringDateHook";

describe("useParseStringDateHook", () => {
  it("should parse the string date was expected", () => {
    const expected = "Friday, February 11th 2022";
    const input = "Feb 11 2022";

    expect(useParseStringDateHook(input)).toEqual(expected);
  });
});
