import { describe, expect, it } from "vitest";
import useAbbreviationDateHook from "../../src/hooks/useAbbreviationDateHook";

describe("useAbbreviationDateHook", () => {
  it("should format the number as expected", () => {
    const expected = "20M";
    const input = "20000000";

    expect(useAbbreviationDateHook(input)).toEqual(expected);
  });
});
