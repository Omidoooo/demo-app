import { describe, expect, it } from "vitest";
import useCurrencyFormatterHook from "../../src/hooks/useCurrencyFormatterHook";

describe("useCurrencyFormatterHook", () => {
  it("should format the currency as expected", () => {
    const expected = "€3,200,000,000.00";
    const input = 3200000000;

    expect(useCurrencyFormatterHook(input)).toEqual(expected);
  });
});
