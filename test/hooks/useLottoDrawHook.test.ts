import { describe, expect, it, vi } from "vitest";
import useLottoDrawHook from "../../src/hooks/useLottoDrawHook";
import GameType from "../../src/types/GameType";
import data from "./drawDataset.json";

describe("useLottoDrawHook", () => {
  it("should fetch the data as expected", async () => {
    // @ts-ignore
    global.fetch = vi.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve(data),
        ok: true,
      })
    );

    const result = await useLottoDrawHook(GameType.EURO_JACKPOT, 1);

    expect(result).toEqual(data.data.draw);
  });
});
