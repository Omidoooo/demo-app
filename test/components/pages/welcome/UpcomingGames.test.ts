import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import UpcomingGames from "../../../../src/components/pages/welcome/UpcomingGames.vue";
import { shallowMount } from "@vue/test-utils";

describe("UpcomingGames", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<UpcomingGames>>;
  const heading: string = "Online Lotto";

  beforeEach(() => {
    wrapper = shallowMount(UpcomingGames);
  });

  it("should be truthy", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should have expected heading", () => {
    expect(wrapper.text()).toContain(heading);
  });
});
