import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import Card from "../../../../../src/components/pages/welcome/upcoming-games/Card.vue";
import { shallowMount } from "@vue/test-utils";

describe("WelcomeCard", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Card>>;
  let date: string = "my date";
  let buttonText: string = "my text";
  let color: string = "rgb(232 121 249)";

  beforeEach(() => {
    wrapper = shallowMount(Card, {
      props: {
        color,
        buttonText,
        date,
      },
    });
  });

  it("should render date", () => {
    expect(wrapper.text()).toContain(date);
  });

  it("should should render button text", () => {
    expect(wrapper.text()).toContain(buttonText);
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
