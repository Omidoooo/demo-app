import { beforeEach, describe, expect, it } from "vitest";
import OddsTable from "./../../../../src/components/pages/winners/OddsTable.vue";
import { shallowMount } from "@vue/test-utils";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import draw from "../draw.json";

describe("OddsTable", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<OddsTable>>;

  beforeEach(() => {
    wrapper = shallowMount(OddsTable, { props: { draw: draw } });
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should contain expected text", () => {
    expect(wrapper.text()).toContain("Winning Odds");
    expect(wrapper.text()).toContain("Class");
    expect(wrapper.text()).toContain("Numbers");
    expect(wrapper.text()).toContain("Sum");
    expect(wrapper.text()).toContain("Winners");
  });
});
