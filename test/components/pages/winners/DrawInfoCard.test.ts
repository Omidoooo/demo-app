import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import DrawInfoCard from "./../../../../src/components/pages/winners/DrawInfoCard.vue";
import { shallowMount } from "@vue/test-utils";
import draw from "../draw.json";

describe("DrawInfoCard", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<DrawInfoCard>>;

  beforeEach(() => {
    wrapper = shallowMount(DrawInfoCard, { props: { draw: draw } });
  });

  it("should match snapshot", () => {
    console.log(wrapper.text());
    expect(wrapper.html()).toMatchSnapshot();
  });
});
