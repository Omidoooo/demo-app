import { it, beforeEach, describe, expect } from "vitest";
import { shallowMount } from "@vue/test-utils";
import DrawnNumbers from "./../../../../../src/components/pages/winners/draw-info-card/DrawnNumbers.vue";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";

describe("DrawnNumbers", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<DrawnNumbers>>;

  beforeEach(() => {
    wrapper = shallowMount(DrawnNumbers, {
      props: {
        additionalNumbers: ["6", "7"],
        numbers: ["1", "2", "3", "4", "99"],
      },
    });
  });

  it("should contain expected text", () => {
    expect(wrapper.text()).toContain("123499 + 67");
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
