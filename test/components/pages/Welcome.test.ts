import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import Welcome from "../../../src/components/pages/Welcome.vue";
import { ComponentPublicInstance } from "vue";
import { shallowMount } from "@vue/test-utils";

describe("Welcome", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Welcome>>;
  const heading: string = "Welcome to Fancy Lotto";
  const welcomeText: string =
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. amet.";

  beforeEach(() => {
    wrapper = shallowMount(Welcome);
  });

  it("should be truthy", () => {
    expect(wrapper).toBeTruthy();
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should have expected text", () => {
    expect(wrapper.text()).toContain(heading);
    expect(wrapper.text()).toContain(welcomeText);
  });
});
