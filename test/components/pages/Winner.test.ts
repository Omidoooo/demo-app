import { beforeEach, describe, expect, it, vi } from "vitest";
import { shallowMount } from "@vue/test-utils";
import Winners from "./../../../src/components/pages/Winners.vue";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import data from "./draw.json";

vi.mock("../../../src/hooks/useLottoDrawHook", () => ({
  __esModule: true,
  default: async () => Promise.resolve({ draws: [data] }),
}));

describe("Winner", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Winners>>;

  beforeEach(() => {
    wrapper = shallowMount(Winners);
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
