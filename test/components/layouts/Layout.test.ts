import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import Layout from "../../../src/components/layouts/Layout.vue";
import { shallowMount } from "@vue/test-utils";
import { createRouter, createWebHistory } from "vue-router";
import { routes } from "../../../src/router";

const router = createRouter({
  history: createWebHistory(),
  routes,
});

describe("Layout", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Layout>>;

  beforeEach(() => {
    wrapper = shallowMount(Layout, {
      global: {
        plugins: [router],
      },
    });
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
