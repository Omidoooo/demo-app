import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import MobileMenu from "../../../../../src/components/layouts/layout/header/MobileMenu.vue";
import { mount } from "@vue/test-utils";
import { createRouter, createWebHistory } from "vue-router";
import { routes } from "../../../../../src/router";

const router = createRouter({
  history: createWebHistory(),
  routes,
});

describe("MobileMenu", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<MobileMenu>>;
  let wrapperClosedMenu: VueWrapper<ComponentPublicInstance<MobileMenu>>;
  const menuItems = [
    {
      name: "Home",
      link: "/",
    },
    {
      name: "Lottery",
      link: "/winners",
    },
  ];

  beforeEach(() => {
    wrapper = mount(MobileMenu, {
      global: {
        plugins: [router],
      },
      props: {
        isMobileMenuOpen: true,
      },
    });

    wrapperClosedMenu = mount(MobileMenu, {
      global: {
        plugins: [router],
      },
      props: {
        isMobileMenuOpen: false,
      },
    });
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should render links correctly", () => {
    for (const menuItem of menuItems) {
      expect(wrapper.text()).toContain(menuItem.name);
      expect(wrapper.html()).toContain(menuItem.link);
    }
  });

  it("should match snapshot if mobile menu is closed", () => {
    expect(wrapperClosedMenu.html()).toMatchSnapshot();
  });
});
