import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import Logo from "../../../../../src/components/layouts/layout/header/Logo.vue";
import { shallowMount } from "@vue/test-utils";

describe("Logo", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Logo>>;
  let slogan: string = "Fancy Lotto";

  beforeEach(() => {
    wrapper = shallowMount(Logo);
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should render slogan", () => {
    expect(wrapper.text()).toContain(slogan);
  });
});
