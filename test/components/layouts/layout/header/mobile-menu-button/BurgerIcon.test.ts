import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import BurgerIcon from "../../../../../../src/components/layouts/layout/header/mobile-menu-button/BurgerIcon.vue";
import { shallowMount } from "@vue/test-utils";

describe("BurgerIcon", () => {
  let wrapperOpen: VueWrapper<ComponentPublicInstance<BurgerIcon>>;
  let wrapperClosed: VueWrapper<ComponentPublicInstance<BurgerIcon>>;

  beforeEach(() => {
    wrapperOpen = shallowMount(BurgerIcon, {
      props: {
        isMobileMenuOpen: true,
      },
    });
    wrapperClosed = shallowMount(BurgerIcon, {
      props: {
        isMobileMenuOpen: false,
      },
    });
  });

  it("should match snapshot if mobile menu is open", () => {
    expect(wrapperOpen.html()).toMatchSnapshot();
  });

  it("should match snapshot if mobile menu is closed", () => {
    expect(wrapperClosed.html()).toMatchSnapshot();
  });
});
