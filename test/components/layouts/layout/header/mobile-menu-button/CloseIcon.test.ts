import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import CloseIcon from "../../../../../../src/components/layouts/layout/header/mobile-menu-button/CloseIcon.vue";
import { shallowMount } from "@vue/test-utils";

describe("CloseIcon", () => {
  let wrapperOpen: VueWrapper<ComponentPublicInstance<CloseIcon>>;
  let wrapperClosed: VueWrapper<ComponentPublicInstance<CloseIcon>>;

  beforeEach(() => {
    wrapperOpen = shallowMount(CloseIcon, {
      props: {
        isMobileMenuOpen: true,
      },
    });
    wrapperClosed = shallowMount(CloseIcon, {
      props: {
        isMobileMenuOpen: false,
      },
    });
  });

  it("should match snapshot if mobile menu is open", () => {
    expect(wrapperOpen.html()).toMatchSnapshot();
  });

  it("should match snapshot if mobile menu is closed", () => {
    expect(wrapperClosed.html()).toMatchSnapshot();
  });
});
