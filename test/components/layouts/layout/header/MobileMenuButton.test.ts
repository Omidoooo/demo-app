import { beforeEach, describe, expect, it, vi } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import MobileMenuButton from "../../../../../src/components/layouts/layout/header/MobileMenuButton.vue";
import { mount } from "@vue/test-utils";

const callbackMock = vi.fn();

describe("MobileMenuButton", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<MobileMenuButton>>;
  const screenReaderText = "Open main menu";

  beforeEach(() => {
    wrapper = mount(MobileMenuButton, {
      props: {
        isMobileMenuOpen: true,
        toggleMobileMenu: callbackMock,
      },
    });
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should render sr text correctly", () => {
    expect(wrapper.text()).toContain(screenReaderText);
  });

  it("should trigger callback when button click", () => {
    wrapper.find("#mobile-menu-button").trigger("click");
    expect(callbackMock.mock.calls.length).toBeTruthy();
  });
});
