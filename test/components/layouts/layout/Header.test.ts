import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import Header from "../../../../src/components/layouts/layout/Header.vue";
import { shallowMount } from "@vue/test-utils";

describe("Header", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Header>>;

  beforeEach(() => {
    wrapper = shallowMount(Header);
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});
