import { beforeEach, describe, expect, it } from "vitest";
import { VueWrapper } from "@vue/test-utils/dist/vueWrapper";
import { ComponentPublicInstance } from "vue";
import Card from "../../../src/components/shared/Card.vue";
import { shallowMount } from "@vue/test-utils";

describe("Card", () => {
  let wrapper: VueWrapper<ComponentPublicInstance<Card>>;
  let title: string = "test title";

  beforeEach(() => {
    wrapper = shallowMount(Card, { props: { title: title } });
  });

  it("should contain title", () => {
    expect(wrapper.text()).toContain(title);
  });

  it("should match snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("should not have an h5 tag if no title is provided", () => {
    const element = shallowMount(Card);
    expect(element.html()).not.contains("h5");
  });
});
