import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

export const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: () => import("./../components/layouts/Layout.vue"),
    children: [
      {
        path: "/",
        component: () => import("../components/pages/Welcome.vue"),
      },
      {
        path: "/winners",
        component: () => import("../components/pages/Winners.vue"),
      },
    ],
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
