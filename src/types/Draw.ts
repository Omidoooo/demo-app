import { GraphqlResult } from "./GraphqlResult";

export interface Draw {
  date: string; // 'Feb 11 2022'
  additionalNumbers: string[]; // [ '3', '10' ],
  gameAmount: string; // '4584808000'
  jackpot: string; // '20000000'
  numbers: string[]; // [ '5', '10', '26', '37', '42' ]
  odds: Odd[];
}

export interface Odd {
  link: string; // '/winnings.pdf'
  numberOfWinners: number; // 1
  numbers: string[]; // ["1","2","3","4","5","6"]
  odd: number; // '26050'
  winningClass: number; // 4
  withOptions: string[]; // example: ["powerplay", "megaplier"]
}

export interface DrawResult extends GraphqlResult {
  draws: Draw[];
}
