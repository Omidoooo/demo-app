export interface GraphqlResult {
  backendError: string;
  success: boolean;
  visibleErrors: VisibleErrors;
}

interface VisibleErrors {
  messages: string[];
  idents: string[];
}
