import moment from "moment";

const useParseStringDateHook = (date: string) => {
  const dateMoment = moment(date, "MMM DD YYYY");
  return dateMoment.format("dddd, MMMM Do YYYY");
};

export default useParseStringDateHook;
