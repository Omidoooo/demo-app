import GameType from "../types/GameType";
import { DrawResult } from "../types/Draw";

const query = `query draws($type: String!, $limit: Int = 10, $date: String) {
  draw(type: $type, limit: $limit, date: $date) {
    backendError
    success
    visibleErrors {
      messages
      idents
    }
    draws {
      date
      time
      video
      additionalNumbers
      gameAmount
      jackpot
      shares
      numbers
      powerPlay
      megaPlier
      odds {
        link
        numberOfWinners
        numbers
        odd
        winningClass
        withOptions
      }
      rollDown
    }
  }
}`;

const useLottoDrawHook = async (
  type: GameType,
  limit: number = 1
): Promise<DrawResult> => {
  const result = await fetch("http://localhost:8010/proxy", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query,
      variables: {
        type,
        limit,
      },
    }),
  });

  const json = await result.json();
  const draw: DrawResult = json?.data.draw;

  if (!draw.success) {
    throw new Error("query was not successful");
  }

  return draw;
};

export default useLottoDrawHook;
