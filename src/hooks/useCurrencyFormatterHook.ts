const currencyFormatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "EUR",
});

const useCurrencyFormatterHook = (amount: number) =>
  currencyFormatter.format(amount);

export default useCurrencyFormatterHook;
