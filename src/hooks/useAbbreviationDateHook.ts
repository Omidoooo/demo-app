const numberAbbreviationFormatter = Intl.NumberFormat("en-US", {
  notation: "compact",
});

const useAbbreviationDateHook = (number: string) =>
  numberAbbreviationFormatter.format(parseInt(number));

export default useAbbreviationDateHook;
